import numpy
from enum import Enum, unique
from typing import Optional


@unique
class Direction(Enum):
    NORTH = 1
    EAST = 2
    SOUTH = 3
    WEST = 4

    @classmethod
    def has_value(cls, value: int) -> bool:
        return value in cls._value2member_map_

    def return_opposite_direction(self):
        if self == self.NORTH:
            return self.SOUTH
        elif self == self.SOUTH:
            return self.NORTH
        elif self == self.EAST:
            return self.WEST
        elif self == self.WEST:
            return self.EAST
        return None


@unique
class MapObject(Enum):
    OBSTACLE = -1
    STARTING_POSITION = -2
    TARGET_POSITION = -3
    PATH = -4


class Robot:
    def __init__(self, input_map: numpy.ndarray, starting_position: tuple, target_position: tuple) -> None:
        self.starting_position: tuple = starting_position
        self.target_position: tuple = target_position
        self.map_size: tuple = input_map.shape
        self.basic_map: numpy.ndarray = input_map.copy()
        self.open_map: numpy.ndarray = numpy.full(shape=self.map_size, fill_value=False, dtype=bool)
        self.closed_map: numpy.ndarray = numpy.full(shape=self.map_size, fill_value=False, dtype=bool)
        self.parents_map: numpy.ndarray = input_map.copy()
        self.cost_map: numpy.ndarray = input_map.copy()

    def calculate_distance(self, position: tuple) -> int:
        distance: int = 0
        while True:
            position = self.find_parent(position=position)
            if position is None:
                break
            distance += 1
        return distance

    def calculate_heuristic(self, position: tuple) -> int:
        position: numpy.ndarray = numpy.array(position, dtype=int)
        return abs(position[0] - self.target_position[0]) + abs(position[1] - self.target_position[1])

    def calculate_path_cost(self, position: tuple) -> int:
        return self.calculate_distance(position=position) + self.calculate_heuristic(position=position)

    def find_neighbour(self, position: tuple, direction: Direction) -> Optional[tuple]:
        neighbour_position: numpy.ndarray = numpy.array(position, dtype=int)
        if direction == Direction.NORTH:
            neighbour_position += numpy.array([-1, 0])
        elif direction == Direction.EAST:
            neighbour_position += numpy.array([0, 1])
        elif direction == Direction.SOUTH:
            neighbour_position += numpy.array([1, 0])
        elif direction == Direction.WEST:
            neighbour_position += numpy.array([0, -1])
        else:
            return None
        if (neighbour_position[0] < 0) or (neighbour_position[0] >= self.map_size[0]):
            return None
        if (neighbour_position[1] < 0) or (neighbour_position[1] >= self.map_size[0]):
            return None
        return tuple(neighbour_position)

    def find_lowest_cost_position(self) -> Optional[tuple]:
        potential_minimum_values = self.cost_map[self.open_map]
        if len(potential_minimum_values) == 0:
            return None
        minimum_value: int = potential_minimum_values.min()
        result = numpy.where(self.cost_map == minimum_value)
        positions = list(zip(result[0], result[1]))
        for position in positions:
            if self.open_map[position]:
                return position
        return None

    def find_parent(self, position: tuple) -> Optional[tuple]:
        parent_value: int = self.parents_map[position]
        if Direction.has_value(parent_value):
            return self.find_neighbour(position=position, direction=Direction(parent_value))
        return None

    def find_path(self, position: tuple) -> Optional[numpy.ndarray]:
        if position == self.target_position:
            return self.return_optimal_path_map()
        self.closed_map[position] = True
        self.open_map[position] = False
        direction: Direction
        for direction in Direction:
            neighbour = self.find_neighbour(position=position, direction=direction)
            if neighbour is None:
                continue
            if self.closed_map[neighbour]:
                continue
            if self.basic_map[neighbour] == MapObject.OBSTACLE.value:
                continue
            old_cost: int = self.cost_map[neighbour]
            old_parent: int = self.parents_map[neighbour]
            self.open_map[neighbour] = True
            self.parents_map[neighbour] = direction.return_opposite_direction().value
            new_cost: int = self.calculate_path_cost(position=neighbour)
            self.cost_map[neighbour] = new_cost
            if 0 < old_cost < new_cost:
                self.cost_map[neighbour] = old_cost
                self.parents_map[neighbour] = old_parent
        position = self.find_lowest_cost_position()
        if position is None:
            return None
        return self.find_path(position=position)

    def return_optimal_path_map(self) -> numpy.ndarray:
        path_map: numpy.ndarray = self.basic_map.copy()
        position: tuple = self.target_position
        while True:
            path_map[position] = MapObject.PATH.value
            position = self.find_parent(position=position)
            if position is None:
                break
        # path_map[self.starting_position] = MapObject.STARTING_POSITION.value
        # path_map[self.target_position] = MapObject.TARGET_POSITION.value
        path_map_bool = path_map.astype(dtype=bool)
        path_map[path_map_bool] = self.parents_map[path_map_bool]
        return path_map


new_map: numpy.ndarray = numpy.zeros((10, 10), dtype=int)

starting_position_01: tuple = (1, 3)
target_position_01: tuple = (8, 8)

# new_map[2, :] = MapObject.OBSTACLE.value  # calkowite odciecie celu od pozycji startowej

new_map[1:8, 4] = MapObject.OBSTACLE.value
new_map[1, 4:-1] = MapObject.OBSTACLE.value
new_map[3, 6:] = MapObject.OBSTACLE.value
new_map[5, 4:-1] = MapObject.OBSTACLE.value
new_map[7, 6:] = MapObject.OBSTACLE.value
new_map[2, 1:5] = MapObject.OBSTACLE.value

robot_01: Robot = Robot(input_map=new_map, starting_position=starting_position_01, target_position=target_position_01)

new_position: numpy.ndarray = numpy.array([1, 3])
print(tuple(new_position + new_position))
print()

print('basic map:\n', new_map)
print('optimal path:\n', robot_01.find_path(robot_01.starting_position))
